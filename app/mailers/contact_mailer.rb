class ContactMailer < ApplicationMailer
  default from: ENV["GMAIL_USERNAME"]
 
  def welcome_email(contact)
    @contact = contact
    mail(to: 'amanda@grahamlegalpa.com', cc: 'lure@keenability.com', subject: 'New Contact for Graham Legal')
    # mail(to: @contact.email, subject: 'New Contact for Graham Legal')
  end
end
