class ContactsController < ApplicationController
  prepend_before_filter :protect_from_spam, :only => [:create, :create_contact]

  def create
    @contact = Contact.new(contact_params)

      if @contact.save
        # Tell the contactMailer to send a welcome email after save
        ContactMailer.welcome_email(@contact).deliver_now
        flash[:success] = "Thank you for contacting us."
        redirect_to root_path(anchor: 'maincontent')
      end
  end

  def create_contact
    @contact = Contact.new(contact_params)

      if @contact.save
        # Tell the contactMailer to send a welcome email after save
        ContactMailer.welcome_email(@contact).deliver_now
        flash[:notice] = "Thank you for contacting us."
        redirect_to root_path(anchor: 'new_contact_2')
      end
  end

  private

  def contact_params
    params.require(:contact).permit(:name, :email, :phone_number, :case_information)
  end
end

