Rails.application.routes.draw do
  root 'home#index'

  match '/contacts'              =>        'contacts#create',              via: [:post]
  match '/create_contact'        =>        'contacts#create_contact',              via: [:post]
end
